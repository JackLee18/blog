---
title: Hand barrage use tutorial
date: 2023-12-08 21:47:27
tags:
---
# Introduction
Hand-held bullet screen *_^ is a useful confession of the artifact, in the subway, bus met you interested in him (she), sorry to open how to do, a useful confession of the artifact, in the subway, bus met you interested in him (she), sorry to open how to, confession of the artifact to help you solve. Whether it is in concert, fans, fans meeting, e-sports scene, airport pick up people, hold up mobile phones, tablets, you can easily get it done and help you solve. Whether it is in concert, fan, fan meeting, e-sports scene, airport pick-up, holding up a mobile phone, tablet, you can easily get it done.

Main functions:

- 1, you can set the size of the text, color, font

- 2, you can set the background color, dynamic background, add background border, select a custom picture as the background

- 3, you can adjust the speed and direction of text scrolling

- 4, mirror function, you can let the text in the mirror normally display rolling, more convenient pick-up

- 5, shooting prompt function, can provide prompt auxiliary function for short video shooting, live broadcast


Download address: ‎ [holding a barrage * _ ^](https://apps.apple.com/cn/app/%E6%89%8B%E6%8C%81%E5%BC%B9%E5%B9%95/id1597042224)


# Set font related


￼![1](2023/12/08/Hand-barrage-use-Tutorial/1.PNG)

## Select a font
Under the Font Related column, select different fonts according to the different shapes of Aa to see the effect
## Select the font color
Under the column of font color, the first color button is the user-defined color, click it will pop up a color selection panel, as follows:

￼![2](2023/12/08/Hand-barrage-use-Tutorial/2.PNG)
￼
The rest of the round buttons in the back are the font colors we carefully selected.
## Select the font size
At the bottom of the font size column, the first button means that the font size is large or small, and the number size on the subsequent button is the corresponding font size
## Select the scroll speed
Check out the screenshot below

￼![3](2023/12/08/Hand-barrage-use-Tutorial/3.PNG)
￼
The first button in the speed column means pause/resume scrolling, the number on the subsequent button is a multiple of the initial scrolling speed, and x represents the meaning of multiple
## Select effect
Under the Effects column, the first button is to cancel the font effect, the second button is the hollow font effect, and the third button is the font shadow effect
## Select scroll direction
The first button below the scroll direction is the default scroll direction, scrolling from the bottom of the screen to the top of the screen; The second button scrolls from the left side of the screen towards the right side of the screen

￼![4](2023/12/08/Hand-barrage-use-Tutorial/4.PNG)

## Other
The first button at the bottom of the other column is the meaning of mirror, which means that the font will be displayed backwards, you put the phone next to the mirror, and the font is positive when you see it in the mirror, this is suitable for some subway, shopping mall pick-up; The second button is the font bold; The third button is font tilt
# Set background related

￼![5](2023/12/08/Hand-barrage-use-Tutorial/5.PNG)
￼
## Set the background color
A button below the background color column is the user's choice to define the background color, and subsequent buttons allow us to carefully select some monochromatic backgrounds and gradient backgrounds
## Dynamic background
Below the animated background column is a set of backgrounds with animated effects, which can be selected to play the animated effects behind the subtitles
￼![6](2023/12/08/Hand-barrage-use-Tutorial/6.PNG)

## Select the background box
The first button below the background box has no background box effect by default, the second button has a white background box, the third button has a black background box, and the fourth button has a rainbow background box
## Select a custom background
There is a plus button below the background of the movie, which is used to select a picture as the background, sometimes you can choose a photo of both sides, or other meaningful photos.
# Set templates related

￼![7](2023/12/08/Hand-barrage-use-Tutorial/7.PNG)

## Color matching template
These are some combinations of background colors and font effects that we have carefully selected
## Dynamic template
These are some combinations of dynamic background and font effects that we have carefully selected
## Save as template
Some of the effects we set ourselves if we feel good, we can click Save as a template, so that in the template column will see "my template" this column, which is our saved custom template 

## Take the cue
The function of shooting prompters is to set the lines, font size, font color, background color, scroll speed, scroll direction, etc. that need to be prompted in advance in the application and then generate the prompter board. Assist users to shoot videos and live broadcasts. In addition, when shooting, you can enlarge or shrink the prompter board by double-clicking the prompter plate, and adjust the volume key to realize the scroll and pause of the text.
A shot prompt can be developed by clicking the shot prompt switch as shown in the picture below. That is, you can use the function, if the switch has no effect, you can switch several times to know that the prompter board can appear.
￼![8](2023/12/08/Hand-barrage-use-Tutorial/8.PNG)
