---
title: pod-publisher-use-Tutorial
date: 2024-02-16 22:35:54
tags:
---

### Background 
 
&emsp; &emsp; Due to historical reasons, the company currently has dozens of pod components, coupled with rapid iteration, a large number of components need to be released after each release is very painful, in order to solve this problem, the development of a visual pod component development tool, the components to be released after the configuration, one click release can be. After all components are published successfully, you can view the release result, a dialog box is displayed, and the cause of the release failure is displayed. 
 
### Implementation principle 
1. Organize the operation commands issued by components and execute them according to the steps. Decide whether to perform subsequent operations according to the execution results of each step, and perform subsequent code synchronization work after component release. 
2. After a component is released, the subsequent components to be released are released. 
 
Functions are as follows: 
1, you can scan the components to be released from the podfile 
2, you can import the components to be released by manual batch copy and paste 
3, component batch release, release success or failure can export release records 
4. After successful publication, you can synchronize the latest component version to podfile 
 
 
### Preparation before use 
1, ensure that the code repository of the component to be released has been cloned to the local 
2. Set up a substitute for ~./ cocoapods folder to facilitate the selection of index warehouse through the substitute in the future 
3. Set up a substitute for the ~./ssh folder to facilitate the submission of the code through ssh clone to the local code repository 
### Import the list of components 
! [insert picture description here] (https://img-blog.csdnimg.cn/7e25e8a956ec40cb95987f533d45fc29.png) 
Open the software and you can see this page. Click the Add button to select the folder of the component (Note: the folder where.podspec is located). After selecting the folder, you can see the data as shown in the figure above. Select one or more components and click the Delete button to remove the selected component import relationship. 
 
### Configure the component list to be published 
&emsp; &emsp; You can import components to be published either manually or by scanning them from the specified podfile. 
1) Select the components to be released on the left, and then click the batch Import button to pop up a new page, as follows: 
! [insert picture description here] (https://img-blog.csdnimg.cn/8dc3886fe5d24273a89d75716af2a518.png) 
You can enter the name of a component, or you can copy and paste the names of a group of components, separated by a newline "\n" (press Enter directly). 
! [insert picture description here] (https://img-blog.csdnimg.cn/f61df5f7250f4942b5ce06e3c23e04e4.png) 
If you want to specify a separate branch here, you can see the <font color=red>JKMusic(featre/0620)</font> format in the image. If the entered component name is not found locally, a corresponding prompt will be displayed. Once confirmed, the selection page closes. As shown in the following figure: The latest version, the release branch these two columns can be modified and edited, if the components to be released are in the same branch, then there is no need to configure the release branch for each component separately. There is a global unified configuration entry. 
! [insert picture description here] (https://img-blog.csdnimg.cn/b534a63f87914b43af7fae7797ea6634.png) 
2) Click on the sidebar to publish components, then select podfile Import at the bottom right, then select the specified podfile file, after selecting a box will prompt you to enter a branch to scan the components to be released. The following steps are the same as 1). 
### Release configuration 
&emsp; &emsp; Click on the side release configuration column, you can see the following interface 
! [insert picture description here] (https://img-blog.csdnimg.cn/86b03e4389b3448cb20230be6a1c3135.png) 
among 
<font color = red>pod index repository </font> : is the pod local index repository. Click the input box on the right to select your own index repository folder. 
<font color =red> Release branch </font> : The release branch of all components in batches (if the release branch of a component is special, you can configure it separately on the component to be released interface) 
<font color = red>develop branch </font> : This is optional. You can configure this option if you need to synchronize code to the develop branch after the component is published. 
<font color=red> Publish </font> : If you have already followed the configuration steps, click the Publish button and wait for the publish result. 
 
### Publish results 
Click the test release result bar, you can see the following interface 
! [insert picture description here] (https://img-blog.csdnimg.cn/0b032f7ca8554e8c8f85ebd2ba66feb7.png) 
After clicking the Publish button, the publish results page will be displayed. After a successful release, the component's name is displayed in green font, and if the release fails, it is displayed in red font. Click the Export Publish Results button to copy the details of the component publish to the paste board, which you can paste in a memo, or send to others to view. This information contains the name of the release component, the version number, and the reason why the release failed. 
 
### Sync the latest version number to podfile 
&emsp; &emsp After selecting side sync to podfile, you can see the following page 
! [insert picture description here] (https://img-blog.csdnimg.cn/50553434eb2a49cbafa8b4f4d82a52c4.png) 
Select the target podfile and click Synchronize to synchronize the release results to the podfile. 
Software download address: https://apps.apple.com/cn/app/pod-publisher/id6443565431?mt=12 
 
Note: You can timely feedback any defects found in the process of use, and I will fix them as soon as possible.

